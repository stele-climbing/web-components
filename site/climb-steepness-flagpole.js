/* 
 * <svg class="climb-steepness-flagpole">
 *     <g class="climb-steepness-flagpole__flags">
 *         <rect class="climb-steepness-flagpole__flag"></rect>
 *     </g>
 *     <line class="climb-steepness-flagpole__pole"></line>
 * </svg>
 */
export default class ClimbSteepnessFlagpole {
  static _counter = 100;
  static nextId(){
    let i = ClimbSteepnessFlagpole._counter;
    ClimbSteepnessFlagpole._counter += 1;
    return i;
  }
  constructor(){
    this._id        = ClimbSteepnessFlagpole.nextId();
    this.height     = 80;
    this.width      = 50;
    this.flagpoleColor = 'black';
    this.flagpoleWidth = 1;
    this.flagpoleOffset   = this.width/2;
    this.d          = []
    this.background = 'rgba(255,255,255,0.1)';
    this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    this.svg.classList.add('climb-steepness-flagpole');
    this.svg.setAttribute('viewBox', [0, 0, this.width, this.height].join(' '));
    this.svg.setAttribute('width', this.width);
    this.svg.setAttribute('height', this.height);
    
    this.gflags = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.gflags.classList.add('climb-steepness-flagpole__flags');
    this.pole = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    this.pole.setAttribute('x1', this.flagpoleOffset);
    this.pole.setAttribute('y1', 0);
    this.pole.setAttribute('x2', this.flagpoleOffset);
    this.pole.setAttribute('y2', this.height);
    this.pole.setAttribute('stroke', this.flagpoleColor ?? 'black');
    this.pole.setAttribute('stroke-width', this.flagpoleWidth);
    this.svg.appendChild(this.gflags);
    this.svg.appendChild(this.pole);
    this.update();
  }
  get container(){ return this.svg; }
  setSteepness(pts){
    this.d = pts.map(pt => [+pt.startingAt, +pt.degreesOverhung]);
    this.redraw();
  }
  getDecorated(){
    let v = [];
    for (let i=0; i<this.d.length; i++) {
      let d = this.d[i][1];
      let a = this.d[i][0];
      let b = this.d[i+1] ? this.d[i+1][0] : 1;
      v.push({
        startingAt: a,
        midpoint: (a + b) / 2,
        stoppingAt: b,
        degreesOverhung: d,
      });
    }
    return v;
  }
  /*
   * this can be replaced to calculate different colors
   */
  flagColor(segment){
    const degrees = segment.degreesOverhung
    return degrees > 0 ? (this.overhangColor ?? 'red') : (this.slabColor ?? 'green')
  }
  flagOffset(d){
    let maxSteep = 90;
    let maxSlab = -50;
    if (d > 0) {
      const leftSpace = this.flagpoleOffset;
      const occupy = Math.min(d, maxSteep) / maxSteep;
      // console.log('left occupy', [d, maxSteep, occupy]);
      return {
        left: leftSpace-(occupy*leftSpace),
        right: this.flagpoleOffset,
      }
    } else {
      const rightSpace = this.width - this.flagpoleOffset;
      const occupy = Math.max(d, maxSlab) / maxSlab;
      // console.log('right occupy', [d, maxSlab, occupy]);
      return {
        left: this.flagpoleOffset,
        right: this.flagpoleOffset + (occupy*rightSpace),
      }
    }
  }
  redraw(){
    const data = this.getDecorated() || [];
    const gflags = this.gflags;
    const flags = [].slice.call(this.gflags.querySelectorAll('.climb-steepness-flagpole__flag'));
    const end = Math.max(data.length,flags.length);
    for (let i=0; i<end; i++) {
      const d = data[i];
      let f = flags[i];
      if (!d) {
        f.remove();
        continue;
      }
      if (!f) {
        f = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        f.classList.add('climb-steepness-flagpole__flag');
        gflags.appendChild(f);
      }
      let offset = this.flagOffset(d.degreesOverhung);
      f.setAttribute('x', offset.left);
      f.setAttribute('width', offset.right-offset.left);
      const top = this.height-(this.height*d.stoppingAt);
      const height = this.height*(d.stoppingAt-d.startingAt);
      f.setAttribute('y', top);
      f.setAttribute('height', height);
      f.setAttribute('fill', this.flagColor(d));
    }
    if (this.backgroundColor)
      this.svg.style.backgroundColor = this.backgroundColor;
    return null;
  }
  update(){
    this.redraw();
    return null;
  }
}
