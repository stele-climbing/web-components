/*
 *
 * <svg class="climb-steepness-dial">
 *   <defs>
 *     <clipPath id="climb-steepness-dial__clip-12345">
 *       <polyline ...></polyline>
 *     </clipPath>
 *   </defs>
 *   <g class="climb-steepness-dial__rings"
 *      clip-path="url(#climb-steepness-dial__clip-12345)">
 *     <circle class="climb-steepness-dial__rings-background" ...></circle>
 *     <circle class="climb-steepness-dial__ring" ...></circle>
 *     <circle class="climb-steepness-dial__ring" ...></circle>
 *     <circle class="climb-steepness-dial__outer-ring" ...></circle>
 *   </g>
 *   <line class="climb-steepness-dial__overhang-limit"></line>
 *   <line class="climb-steepness-dial__slab-limit"></line>
 *   <g class="climb-steepness-dial__rays">
 *     <line class="climb-steepness-dial__ray"></line>
 *     <line class="climb-steepness-dial__ray"></line>
 *   </g>
 * </svg>
 *
 */

/*
 *
 * const d = new ClimbSteepnessDial();
 * d.setSteepness([{startingAt: 0, degreesOverhung: 45}]);
 * document.body.appendChild(d.container);
 */
export default class ClimbSteepnessDial {

  constructor(){
    this.r               = 50;
    this.overhangMax     = +90;
    this.overhangMin     = -70;
    this.outlineWidth    = 2;
    this.outlineColor    = 'gray';
    this.backgroundColor = 'transparent';
    this.rayWidth        = 4;
    this.ringFill        = 'transparent';
    this.ringWidth       = 0.5;
    this.ringColor       = 'rgba(0,0,0,0.3)';
    this.ringDasharray   = '2 2';

    this._id = ClimbSteepnessDial.nextId();
    this.d   = []
    const cx = this.r;
    const cy = this.r;

    const ohMinRun  = Math.sin(this.overhangMin * (Math.PI/180));
    const ohMinRise = Math.cos(this.overhangMin * (Math.PI/180));
    const ohMaxRun  = Math.sin(this.overhangMax * (Math.PI/180));
    const ohMaxRise = Math.cos(this.overhangMax * (Math.PI/180));

    const tca = (tag,clss,attrs) => {
      const el = document.createElementNS('http://www.w3.org/2000/svg', tag);
      if (clss) el.classList.add(clss);
      for (const [a,v] of Object.entries(attrs)) 
        el.setAttribute(a, v);
      return el;
    }

    this.svg = tca('svg', 'climb-steepness-dial', {
      viewBox:  `0 0 ${this.r*2} ${this.r}`,
      width:    `${this.r*2}px`,
      height:   `${this.r}px`,
      style: 'overflow: visible;',
    });
    this.defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
    this.ringsClip = tca('clipPath', null, {
      id: `climb-steepness-dial__clip-${this._id}`
    });
    this.ringsClipPolyline = tca('polyline', null, {
      points: [[-5,                     -5],
               [cx - (ohMaxRun*(this.r*1.1)), cy - (ohMaxRise*(this.r*1.1))],
               [cx,                     cy],
               [cx - (ohMinRun*(this.r*1.1)), cy - (ohMinRise*(this.r*1.1))],
               [this.r*2,               -5]
              ].map(p => p.join(',')).join(' ')
    });
    this.raysg = tca('g', 'climb-steepness-dial__rays', {});
    this.ringsg = tca('g', 'climb-steepness-dial__rings', {
      'clip-path': `url(#climb-steepness-dial__clip-${this._id})`,
    });
    this.outerRing = tca('circle', 'climb-steepness-dial__outer-ring', {
      cx: cx,
      cy: cy,
      r:  this.r,
    });
    this.styleOutline(this.outerRing);
    this.ringsBg = tca('circle', 'climb-steepness-dial__rings-background', {
      cx: cx,
      cy: cy,
      r: this.r,
      style: `fill: ${this.backgroundColor ?? 'white'}; stroke-width: 0;`,
    })
    const rayOhMin = tca('line', 'climb-steepness-dial__slab-limit', {
      x1: cx,
      y1: cy,
      x2: cx - (ohMinRun * this.r),
      y2: cy - (ohMinRise * this.r),
    });
    this.styleOutline(rayOhMin);
    const rayOhMax = tca('line', 'climb-steepness-dial__overhang-limit', {
      x1: cx,
      y1: cy,
      x2: cx - (ohMaxRun * this.r),
      y2: cy - (ohMaxRise * this.r),
    });
    this.styleOutline(rayOhMax);

    //
    // indented only for clarity of nesting
    //
    this.svg.appendChild(this.defs);
      this.defs.appendChild(this.ringsClip);
        this.ringsClip.appendChild(this.ringsClipPolyline);
      this.svg.appendChild(this.ringsg);
        this.ringsg.appendChild(this.outerRing);
        this.ringsg.appendChild(this.ringsBg);
      this.svg.appendChild(rayOhMax);
      this.svg.appendChild(rayOhMin);
      this.svg.appendChild(this.raysg);

    this.update();
  }

  get container(){ return this.svg; }

  /*
   * these svgs are going to be placed inline.
   * without unique ids on the clip path,
   * the clipPath of the first dial will be used
   * for every subsequent dial in the document
   */
  static _counter = 100; // unique ids are needed for clipping 
  static nextId(){
    let i = ClimbSteepnessDial._counter;
    ClimbSteepnessDial._counter += 1;
    return i;
  }

  /*
   * the style functions below don't do anything magic.
   * they isolate the styling logic.
   * for styling changes beyond replacing the existing object properties
   * these methods can be overridden with no other consequence
   */
  styleOutline(el){
    el.style.strokeLinecap = 'round';
    el.style.fill          = 'none';
    el.style.stroke        = this.outlineColor;
    el.style.strokeWidth   = this.outlineWidth;
  }
  styleRay(el, segmentData){
    el.style.fill          = 'none';
    el.style.strokeLinecap = 'round';
    el.style.strokeWidth   = this.rayWidth;
    el.style.stroke        = (segmentData.degreesOverhung < 0) ? 'green' :
                             (0 < segmentData.degreesOverhung) ? 'red' :
                             'black';
  }
  styleRing(el, segmentData){
    el.style.fill            = this.ringFill;
    el.style.strokeWidth     = this.ringWidth;
    el.style.strokeDasharray = this.ringDasharray;
    el.style.stroke          = this.ringColor;
  }


  /*
   * the object is translated to effectively call-by-value
   * hanging onto the original object would create an 
   * unnecessary opportunity to affect the "outside" world
   */
  setSteepness(pts){
    this.d = [];
    for (let pt of pts)
      this.d.push([+pt.startingAt, +pt.degreesOverhung])
    this.redraw();
    return this;
  }
 
  /*
   * attach some extra data to the basic [startingAt, degreesOverhung]
   */
  getDecorated(){
    let v = [];
    for (let i=0; i<this.d.length; i++) {
      let o = this.d[i][1];
      let a = this.d[i][0];
      let b = this.d[i+1] ? this.d[i+1][0] : 1;
      v.push({
        startingAt: a,
        stoppingAt: b,
        degreesOverhung: o,
        radiansOverhung: o * (Math.PI / 180),
      });
    }
    return v;
  }

  redraw(){
    const r  = this.r;
    const cx = r;
    const cy = r;
    const segments = this.getDecorated() || [];
    const rays     = this.raysg.querySelectorAll('.climb-steepness-dial__ray');
    const rings    = this.ringsg.querySelectorAll('.climb-steepness-dial__ring');
    const end      = Math.max(segments.length,rays.length);
    for (let i=0; i<end; i++) {
      let seg = segments[i];
      let ray = rays[i];
      let ring = rings[i];
      if (!seg) {
        ray.remove();
        ring.remove();
        continue;
      }
      if (!ray) {
        ray = document.createElementNS('http://www.w3.org/2000/svg', 'line');
        ray.classList.add('climb-steepness-dial__ray');
        this.styleRay(ray, seg);
        this.raysg.appendChild(ray);
        ring = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        this.styleRing(ring, seg);
        ring.classList.add('climb-steepness-dial__ring');
        this.ringsg.appendChild(ring);
      }
      ring.setAttribute('cx', cx);
      ring.setAttribute('cy', cy);
      let run = Math.sin(seg.radiansOverhung);
      let rise = Math.cos(seg.radiansOverhung);
      let start = seg.startingAt;
      let stop = seg.stoppingAt;
      ray.setAttribute('x1', cx - (start*run)*r);
      ray.setAttribute('y1', cy - (start*rise)*r);
      ray.setAttribute('x2', cx - (stop*run)*r);
      ray.setAttribute('y2', cy - (stop*rise)*r);
      ring.setAttribute('r', stop * r);
    }
    this.ringsg.appendChild(this.outerRing); // ensure that the outer ring is on top
    return null;
  }
  update(){
    this.redraw();
    return null;
  }
}
