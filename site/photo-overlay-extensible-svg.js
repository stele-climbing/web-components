export default class PhotoOverlayExtensibleSvg {
/*
  climbNameSize = 30
  areaNameSize = 30
*/
  constructor(url){
    this.url = url;
    this.areas = [];
    this.climbs = [];
    this.container = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    this.areasg    = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.climbsg   = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.image     = document.createElementNS('http://www.w3.org/2000/svg', 'image');
    this.container.classList.add('photo-overlay-extensible-svg');
    this.image    .classList.add('photo-overlay-extensible-svg__image');
    this.areasg   .classList.add('photo-overlay-extensible-svg__areas');
    this.climbsg  .classList.add('photo-overlay-extensible-svg__climbs');
    this.container.appendChild(this.image);
    this.container.appendChild(this.areasg);
    this.container.appendChild(this.climbsg);
    this.initSize().then(v => this.redraw());
  }
  redraw(){
    if (!this.size) {
      console.info('postponing drawing; there is no size');
      return;
    }
    const w = this.size.width;
    const h = this.size.height;
    this.container.setAttributeNS(null, 'viewBox', `0 0 ${w} ${h}`);
    this.image.setAttributeNS(null, 'href', this.url);
    this.image.setAttributeNS(null, 'x',      0);
    this.image.setAttributeNS(null, 'y',      0);
    this.image.setAttributeNS(null, 'width',  w);
    this.image.setAttributeNS(null, 'height', h);
    this.climbsg.innerHTML = '';
    for (let [name, url, path] of this.climbs) {
      const c = this.drawClimb(name, url, path);
      this.climbsg.appendChild(c);
    }
    this.areasg.innerHTML = '';
    for (let [name, url, perimeter] of this.areas) {
      const c = this.drawArea(name, url, perimeter);
      this.areasg.appendChild(c);
    }
  }
  drawClimb(name, url, path) {
    const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    const a = document.createElementNS('http://www.w3.org/2000/svg', 'a');
    const p = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    g.classList.add('photo-overlay-extensible-svg__climb');
    a.classList.add('photo-overlay-extensible-svg__climb-link');
    p.classList.add('photo-overlay-extensible-svg__climb-path');
    a.setAttributeNS(null, 'href', url);
    const d = 'M ' + path.map(([x, y]) => {
      const left =       (x / 100) * this.size.width;
      const top  = ((100-y) / 100) * this.size.height;
      return `${left} ${top}`
    }).join(' L ');
    p.setAttributeNS(null, 'd', d)
    g.appendChild(a)
    a.appendChild(p)

/*
    // TEXT TEST
    const txtg  = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    txtg.classList.add('photo-overlay-extensible-svg__climb-label');
    const txt   = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    const txtbg = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    txtbg.classList.add('photo-overlay-extensible-svg__climb-label-background');
    txtbg.setAttributeNS(null, 'x', 0);
    txtbg.setAttributeNS(null, 'y', 0);
    txtbg.setAttributeNS(null, 'width',  this.size.width);
    txtbg.setAttributeNS(null, 'height', this.climbNameSize);
    txtbg.setAttributeNS(null, 'fill',   'black');
    txt.classList.add('photo-overlay-extensible-svg__climb-label-name');
    txt.setAttributeNS(null, 'x', this.size.width/2);
    txt.setAttributeNS(null, 'y', this.climbNameSize/2);
    txt.style.fontSize = this.climbNameSize + 'px';
    txt.style.textAnchor = 'middle';
    txt.style.dominantBaseline = 'central';
    txt.style.fill = 'white';
    txt.textContent = name;
    g.appendChild(txtg)
    txtg.appendChild(txtbg)
    txtg.appendChild(txt)
*/

    return g;
  }
  drawArea(name, url, perimeter) {
    const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    const a = document.createElementNS('http://www.w3.org/2000/svg', 'a');
    const p = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    g.classList.add('photo-overlay-extensible-svg__area');
    a.classList.add('photo-overlay-extensible-svg__area-link');
    p.classList.add('photo-overlay-extensible-svg__area-perimeter');
    a.setAttributeNS(null, 'href', url);
    const d = 'M ' + perimeter.map(([x, y]) => {
      const left =       (x / 100) * this.size.width;
      const top  = ((100-y) / 100) * this.size.height;
      return `${left} ${top}`
    }).join(' L ') + ' Z';
    p.setAttributeNS(null, 'd', d)
    const txtg  = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    txtg.classList.add('photo-overlay-extensible-svg__area-label');
    const txt   = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    const txtbg = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    txtbg.classList.add('photo-overlay-extensible-svg__area-label-background');
    txtbg.setAttributeNS(null, 'x', 0);
    txtbg.setAttributeNS(null, 'y', 0);
    txtbg.setAttributeNS(null, 'width',  this.size.width);
    txtbg.setAttributeNS(null, 'height', this.areaNameSize);
    txtbg.setAttributeNS(null, 'fill',   'black');
    txt.classList.add('photo-overlay-extensible-svg__area-label-name');
    txt.setAttributeNS(null, 'x', this.size.width/2);
    txt.setAttributeNS(null, 'y', this.areaNameSize/2);
    txt.style.fontSize = this.areaNameSize + 'px';
    txt.style.textAnchor = 'middle';
    txt.style.dominantBaseline = 'central';
    txt.style.fill = 'white';
    txt.textContent = name;
    g.appendChild(a)
    a.appendChild(p)
    g.appendChild(txtg)
    txtg.appendChild(txtbg)
    txtg.appendChild(txt)
    return g;
  }
  addClimb(name, url, path){
    this.climbs.push([name, url, path]);
  }
  addArea(name, url, perimeter){
    this.areas.push([name, url, perimeter]);
  }
  async initSize(){
    const loadImageSize = function(url){
      return new Promise(function(resolve, reject){
        const img = document.createElement('img');
        img.onload = (e) => {
          resolve({
            width: e.target.naturalWidth,
            height:  e.target.naturalHeight,
          });
        };
        img.setAttribute('src', url);
      });
    };
    this.size = await loadImageSize(this.url);
    return this;
  }
}

