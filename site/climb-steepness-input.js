/*
 * Here's the layout:
 *
 * <div class="climb-steepness-input">
 *     <div class="climb-steepness-input--segments">
 *         <div class="climb-steepness-input--segment">
 *             <input  class="climb-steepness-input--starting-at">
 *             <input  class="climb-steepness-input--degrees-overhung">
 *             <button class="climb-steepness-input--remove">
 *             <button class="climb-steepness-input--divide">
 *         </div>
 *     </div>
 *     <div class="climb-steepness-input--general">
 *         <button class="climb-steepness-input--clear">
 *         <button class="climb-steepness-input--start">
 *     </div>
 * </div>
 *
 */
export default class ClimbSteepnessInput {
    overhangMax = +90
    overhangMin = -90
    constructor(){
        this.container = document.createElement('div');
        this.segments  = document.createElement('div');
        this.general   = document.createElement('div');
        this.clear     = document.createElement('button');
        this.start     = document.createElement('button');
        this.container.classList.add('climb-steepness-input');
        this.segments .classList.add('climb-steepness-input--segments');
        this.general  .classList.add('climb-steepness-input--general');
        this.clear    .classList.add('climb-steepness-input--clear');
        this.start    .classList.add('climb-steepness-input--start');
        this.clear.textContent = 'clear';
        this.start.textContent = 'add segment';
        this.container.appendChild(this.segments);
        this.container.appendChild(this.general);
        this.general.appendChild(this.clear);
        this.general.appendChild(this.start);
        this.segments.appendChild(this.createSegmentElement())
    }
    static splitSegment(segments, startingAt){
        //NOTE: perhaps this should only split if there is enough space height=2*minstep
        let result = [];
        for (let i = 0; i<segments.length; i++) {
            if (segments[i].startingAt === startingAt) {
                const nextStart = (i+1 < segments.length) ? segments[i+1].startingAt : 1;
                const startingAt = segments[i].startingAt;
                const degreesOverhung = segments[i].degreesOverhung;
                result.push({startingAt,                               degreesOverhung});
                result.push({startingAt: (startingAt + nextStart) / 2, degreesOverhung});
            } else {
                result.push({...segments[i]});
            }
        }
        return result;
    }
    static dropSegment(segments, startingAt){
        const s2 = segments.filter(seg => seg.startingAt !== startingAt).map(seg => ({...seg}));
        if (!s2.length) {
            return null;
        } else {
            s2[0].startingAt = 0; // in case we removed the first segment
            return s2;
        }
    }
    createSegmentElement(){
        const segment = document.createElement('div');
        const startAt = document.createElement('input');
        const degrees = document.createElement('input');
        const divide  = document.createElement('button');
        const remove  = document.createElement('button');
        segment.classList.add('climb-steepness-input--segment');
        startAt.classList.add('climb-steepness-input--starting-at');
        degrees.classList.add('climb-steepness-input--degrees-overhung');
        divide .classList.add('climb-steepness-input--divide');
        remove .classList.add('climb-steepness-input--remove');
        startAt.setAttribute('type', 'number');
        startAt.setAttribute('step', '0.05');
        startAt.setAttribute('min',  '0.00');
        startAt.setAttribute('max',  '0.95');
        degrees.setAttribute('type', 'number');
        degrees.setAttribute('step', '5');
        degrees.setAttribute('min',  '-90');
        degrees.setAttribute('max',  '100');
        divide.textContent  = "split"; // "&#9986;" // (scissors) // &plus;
        remove.textContent = "remove"; // "&times;"
        divide.onclick = (e) => {
            let a = this.getValue()
            let b = ClimbSteepnessInput.splitSegment(a, Number(startAt.value));
            this.redraw(b);
            this.fireInput();
        }
        remove.onclick = (e) => {
            let a = this.getValue()
            let b = ClimbSteepnessInput.dropSegment(a, Number(startAt.value));
            this.redraw(b);
            this.fireInput();
        }
        startAt.oninput = (e) => {
            this.redraw(this.getValue());
            this.fireInput();
        }
        degrees.oninput = (e) => {
            this.redraw(this.getValue());
            this.fireInput();
        }
        segment.appendChild(startAt);
        segment.appendChild(degrees);
        segment.appendChild(divide);
        segment.appendChild(remove);
        return segment;
    }
    fireInput(){
        if (this.oninput) this.oninput(this.getValue());
    }
    static updateSegmentElement(el, segmentData, context) {
        const segment = el; 
        const startAt = el.querySelector('.climb-steepness-input--starting-at');
        const degrees = el.querySelector('.climb-steepness-input--degrees-overhung');
        // const divide  = el.querySelector('.climb-steepness-input--divide');
        // const remove  = el.querySelector('.climb-steepness-input--remove');
        startAt.value = segmentData.startingAt;
        if (context.prevStart === null) {
          startAt.value = 0;
          startAt.setAttribute('disabled', 'disabled');
        } else {
          startAt.removeAttribute('disabled');
        }
        const min = (context.prevStart !== null) ? (context.prevStart+0.05) : 0;
        const max = (context.nextStart !== null) ? (context.nextStart-0.05) : 0.95;
        startAt.setAttribute('min', min);
        startAt.setAttribute('max', max);
        degrees.value = segmentData.degreesOverhung;
    }
    redrawInputs(data){
        const elements = [].slice.call(this.segments.querySelectorAll('.climb-steepness-input--segment')).reverse();
        const end = Math.max(data.length, elements.length);
        for (let i=0; i<end; i++) {
            let e = elements[i];
            let d = data[i];
            if (!d) {
                e.remove();
            } else {
                if (!e) {
                    e = this.createSegmentElement();
                    if (this.segments.children.length) 
                        this.segments.insertBefore(e, this.segments.children[0]);
                    else
                        this.segments.appendChild(e);
                }
                //TODO: update
                ClimbSteepnessInput.updateSegmentElement(e, data[i], {
                    prevStart: (i === 0) ? null : data[i-1].startingAt,
                    nextStart: (i+1===data.length) ? null : data[i+1].startingAt,
                })
            }
        }
        return null;
    }
    redraw(v){
        if (v && v.length) {
            this.general.setAttribute('hidden', 'hidden');
            this.segments.removeAttribute('hidden');
            this.redrawInputs(v);
        } else {
            this.segments.setAttribute('hidden', 'hidden');
            this.general.removeAttribute('hidden');
        }
    }
    setValue(v){
        this.redraw(v);
    }
    getValue(){
        const q = this.segments.querySelectorAll('.climb-steepness-input--segment');
        const elements = [].slice.call(q).reverse();
        return elements.map(e => {
            const startEl = e.querySelector('.climb-steepness-input--starting-at');
            const degreesEl = e.querySelector('.climb-steepness-input--degrees-overhung');
            return {
                startingAt: Number(startEl.value),
                degreesOverhung: Number(degreesEl.value),
            }
        })
    }
    update(){
        let valid = true;
        if (valid && this.onchange)
            this.onchange(this.getValue());
        this.redraw();
        return null;
    }
}
