/*
 *  -------------------- CALLING THIS ---------------------
 * 
 *  photoOverlayStaticSvg({
 *     url: './el-cap.jpg',
 *     size: [1000,750],
 *     climbs: [{
 *         name: 'The Nose',
 *         url:  'http://my-climbing-platform.com/climb/the-nose',
 *         path: [[40,10],[30,50],[27,90]],
 *         style: {strokeWidth:5,stroke:'orange'},
 *     }],
 *     areas: [{
 *         name:      'The Dawn Wall',
 *         url:       'http://my-climbing-platform.com/area/the-dawn-wall',
 *         perimeter: [[50,10],[80,10],[80,75],[50,90]],
 *         style:     {strokeWidth:5,stroke:'pink'},
 *     }],
 * });
 *
 * --------------- RETURNS AN SVG ELEMENT -----------------------
 *
 *  <svg class="photo-overlay" viewBox="0 0 1000 750">
 *      <image href="./el-cap.jpg" x="0" y="0" width="1000" height="750"></image>
 *      <a class="photo-overlay-climb"
 *         href="http://my-climbing-platform.com/climb/the-nose"
 *         title="The Nose">
 *          <path d="M 400 675 L 300 375 L 270 75"
 *                style="fill:            none;
 *                       stroke-linecap:  round;
 *                       stroke-linejoin: round;
 *                       stroke:          orange;
 *                       stroke-width:    5px;"></path></a>
 *      <a class="photo-overlay-area"
 *         href="http://my-climbing-platform.com/area/the-dawn-wall" title="The Dawn Wall">
 *          <path d="M 500 675 L 800 675 L 800 187.5 L 500 75 Z"
 *                style="fill:            none;
 *                       stroke-linecap:  round;
 *                       stroke-linejoin: round;
 *                       stroke:          pink;
 *                       stroke-width:    5px;"></path></a>
 *  </svg>
 *
 */
export function photoOverlayStaticSvg(cfg) {
    const url    = cfg.url;
    const [w,h]  = cfg.size;
    const coords = pts => pts.map(([x,y]) => ([(x/100)*w, ((100-y)/100)*h]));
    const climbs = cfg.climbs || [];
    const areas  = cfg.areas || [];
    const svg    = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const img    = document.createElementNS('http://www.w3.org/2000/svg', 'image');
    svg.classList.add('photo-overlay');
    svg.appendChild(img);
    svg.setAttributeNS(null, 'viewBox', `0 0 ${w} ${h}`);
    img.setAttributeNS(null, 'href',    url);
    img.setAttributeNS(null, 'x',       0);
    img.setAttributeNS(null, 'y',       0);
    img.setAttributeNS(null, 'width',   w);
    img.setAttributeNS(null, 'height',  h);
    for (let climb of climbs) {
        const a = document.createElementNS('http://www.w3.org/2000/svg', 'a');
        a.classList.add('photo-overlay-climb');
        const p = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        a.setAttributeNS(null, 'href', climb.url);
        a.setAttributeNS(null, 'title', climb.name);
        const d = 'M ' + coords(climb.path).map(pt => pt.join(' ')).join(' L ');
        p.setAttributeNS(null, 'd', d)
        p.style.fill           = 'none';
        p.style.strokeLinecap  = 'round';
        p.style.strokeLinejoin = 'round';
        p.style.stroke         = 'black'
        p.style.strokeWidth    = 10;
        for (let k in (climb.style||{})) p.style[k] = climb.style[k];
        a.appendChild(p);
        svg.appendChild(a);
    }
    for (let area of areas) {
        const a = document.createElementNS('http://www.w3.org/2000/svg', 'a');
        const p = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        a.classList.add('photo-overlay-area');
        a.setAttributeNS(null, 'href', area.url);
        a.setAttributeNS(null, 'title', area.name);
        const d = 'M ' + coords(area.perimeter).map(pt => pt.join(' ')).join(' L ') + ' Z';
        p.setAttributeNS(null, 'd', d)
        p.style.fill           = 'none';
        p.style.strokeLinecap  = 'round';
        p.style.strokeLinejoin = 'round';
        p.style.stroke         = 'black'
        p.style.strokeWidth    = 7;
        for (let k in (area.style||{})) p.style[k] = area.style[k];
        a.appendChild(p);
        svg.appendChild(a);
    }
    return svg;
}

/*
 * const url  = 'el-cap.jpg';
 * const size = await getImageSizeFromUrl(url);
 * photoOverlayStaticSvg({
 *     url,
 *     size,
 *     climbs: [{
 *         name: 'The Nose',
 *         url:  '/climb/the-nose',
 *         path: [[20,80],[25,50],[27,20]],
 *     }],
 * });
 *
 */
export function getImageSizeFromUrl(url){
    return new Promise((resolve, reject) => {
        const img = document.createElement('img');
        img.onload = (e) => {
            resolve([e.target.naturalWidth, e.target.naturalHeight]);
        };
        img.setAttribute('src', url);
    });
}

export default photoOverlayStaticSvg;
