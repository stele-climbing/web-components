export default class ClimbSectionsBars {
  constructor(){
    this.height   = 180;
    this.padding  = 5;
    this.width    = 80;
    this.radius   = 2;
    this.barGap   = 4;
    this.barWidth = 6;
    this.textGap  = 3;
    this.fontSize = 12;
    this.d = []
    this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    this.svg.style.overflow = 'visible';
    this.svg.classList.add('climb-sections-bars');
    this.svg.setAttribute('viewBox', `0 0 ${this.width} ${this.height}`);
    this.svg.setAttribute('width',   this.width);
    this.svg.setAttribute('height',  this.height);
    this.segments = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.segments.classList.add('climb-sections-segments');
    this.svg.appendChild(this.segments);
    this.update();
  }
  get container(){ return this.svg; }
  setSections(pts){
    this.d = pts.map(pt => [+pt.startingAt, pt.description]);
    this.redraw();
  }
  getDecorated(){
    let result = [];
    for (let i=0; i<this.d.length; i++) {
      let a = this.d[i][0];
      let b = this.d[i+1] ? this.d[i+1][0] : 1;
      result.push({
        startingAt: a,
        midpoint: (a + b) / 2,
        stoppingAt: b,
        description: this.d[i][1],
      });
    }
    return result;
  }
  redraw(){
    const data = this.getDecorated() || [];
    const vg = this.segments;
    const els = [].slice.call(this.segments
                                .querySelectorAll('.climb-sections-segment'));
    const end = Math.max(data.length,els.length);
    for (let i=0; i<end; i++) {
      const d = data[i];
      let   e = els[i];
      if (!d) {
        e.remove();
        continue;
      }
      if (!e) {
        e = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        e.classList.add('climb-sections-segment');
        const txt = document.createElementNS('http://www.w3.org/2000/svg', 'text');
        txt.style.textAnchor = 'left';
        txt.style.dominantBaseline = 'middle';
        txt.classList.add('climb-sections-segment__text');
        txt.setAttribute('font-size', this.fontSize);
        const bar = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        bar.classList.add('climb-sections-segment__bar');
        bar.setAttribute('fill',         'black');
        bar.setAttribute('stroke-width', '0');
        e.appendChild(txt);
        e.appendChild(bar);
        vg.appendChild(e);
      }

      const txt = e.querySelector('text');
      txt.innerHTML = '';
      txt.appendChild(document.createTextNode(d.description));
      txt.setAttribute('x', this.barWidth + this.textGap);
      txt.setAttribute('y', this.height - (this.height * d.midpoint));
      const bar = e.querySelector('rect');

      const top    = this.height - (this.height*d.stoppingAt) + (this.barGap/2)
      const height = this.height * (d.stoppingAt - d.startingAt) - this.barGap;
      bar.setAttribute('x',      0);
      bar.setAttribute('y',      top);
      bar.setAttribute('width',  this.barWidth);
      bar.setAttribute('height', height);
      bar.setAttribute('ry',     this.radius);
      bar.setAttribute('rx',     this.radius);
      e.dataset.startingAt  = d.startingAt;
      e.dataset.stoppingAt  = d.stoppingAt;
      e.dataset.description = d.description;
    }
    return null;
  }
  update(){
    this.redraw();
    return null;
  }
}
