/*
 * <svg class="climb-steepness-icon">
 *   <defs>
 *     <clipPath id="climb-steepness-icon__scene-clip-12345">
 *       <rect ...></rect>
 *     </clipPath>
 *   </defs>
 *   <g class="climb-steepness-icon__scene"
 *      clip-path="url(#climb-steepness-icon__scene-clip-12345)">
 *     <rect class="climb-steepness-icon__sky" ...></rect>
 *     <rect class="climb-steepness-icon__rock" ...></rect>
 *   </g>
 *   <rect class="climb-steepness-icon__border" ...></rect>
 * </svg>
 */
export default class ClimbSteepnessIcon {
    static _counter = 100;
    static nextId(){
        const i = ClimbSteepnessIcon._counter;
        ClimbSteepnessIcon._counter += 1;
        return i;
    }
    width        = 50
    height       = 50
    borderWidth  = 3
    borderRadius = 5
    borderColor  = '#222222'
    rockColor    = '#A0522D' // sienna
    skyColor     = '#B0E0E6' // powder blue
    constructor(){
        this.id     = ClimbSteepnessIcon.nextId();
        this.svg    = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        this.border = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        this.sky    = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        this.rock   = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        this.defs   = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
        this.scene  = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        this.clipp  = document.createElementNS('http://www.w3.org/2000/svg', 'clipPath');
        this.clipr  = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        this.svg.classList.add('climb-steepness-icon');
        this.border.classList.add('climb-steepness-icon__border');
        this.sky.classList.add('climb-steepness-icon__sky');
        this.rock.classList.add('climb-steepness-icon__rock');
        this.scene.classList.add('climb-steepness-icon__scene');
        this.clipp.setAttributeNS(null, 'id', `climb-steepness-icon__clip-${this.id}`);
        this.scene.setAttributeNS(null, 'clip-path', `url(#climb-steepness-icon__clip-${this.id})`);
        this.border.style.fill = "none";
        this.svg.appendChild(this.defs);
        this.defs.appendChild(this.clipp);
        this.clipp.appendChild(this.clipr);
        this.scene.appendChild(this.sky)
        this.scene.appendChild(this.rock);
        this.svg.appendChild(this.scene);
        this.svg.appendChild(this.border);
    }
    updateContainerSize(d, size){
        const width       = size?.[0]          ?? this.width;
        const height      = size?.[1]          ?? this.height;
        this.svg.setAttributeNS(null, 'width',   width);
        this.svg.setAttributeNS(null, 'height',  height);
        this.svg.setAttributeNS(null, 'viewBox', `0 0 ${width} ${height}`);
    }
    updateContentLayout(d, size, style){
        const borderWidth = style?.borderWidth ?? this.borderWidth;
        const width       = size?.[0]          ?? this.width;
        const height      = size?.[1]          ?? this.height;
        const diagonal    = Math.sqrt((width+borderWidth)**2+(height+borderWidth)**2);
        this.sky.setAttributeNS(null, 'width',  width);
        this.sky.setAttributeNS(null, 'height', height);
        this.sky.setAttributeNS(null, 'x', 0);
        this.sky.setAttributeNS(null, 'y', 0);

        this.rock.setAttributeNS(null, 'width',  diagonal);
        this.rock.setAttributeNS(null, 'height', diagonal);
        this.rock.setAttributeNS(null, 'x', width / 2);
        this.rock.setAttributeNS(null, 'y', (height-diagonal)/2);
        this.rock.setAttributeNS(null, 'transform', `rotate(${d*-1} ${width/2} ${height/2})`);
        this.clipr.setAttributeNS(null, 'width',  width - borderWidth);
        this.clipr.setAttributeNS(null, 'height', height - borderWidth);
        this.clipr.setAttributeNS(null, 'x', borderWidth / 2);
        this.clipr.setAttributeNS(null, 'y', borderWidth / 2);
        this.clipr.setAttributeNS(null, 'rx', style?.borderRadius ?? this.borderRadius);
        this.clipr.setAttributeNS(null, 'ry', style?.borderRadius ?? this.borderRadius);

        this.border.setAttributeNS(null, 'width',  width - borderWidth);
        this.border.setAttributeNS(null, 'height', height - borderWidth);
        this.border.setAttributeNS(null, 'x', borderWidth / 2);
        this.border.setAttributeNS(null, 'y', borderWidth / 2);
        this.border.setAttributeNS(null, 'rx', style?.borderRadius ?? this.borderRadius);
        this.border.setAttributeNS(null, 'ry', style?.borderRadius ?? this.borderRadius);
    }
    updateColors(opts){
        this.border.style.stroke      = opts?.borderColor ?? this.borderColor;
        this.border.style.strokeWidth = opts?.borderWidth ?? this.borderWidth;
        this.sky.style.fill           = opts?.skyColor    ?? this.skyColor;
        this.rock.style.fill          = opts?.rockColor   ?? this.rockColor;
    }
    redraw(d, size, opts){
        this.updateContainerSize(d, size);
        this.updateContentLayout(d, size, opts);
        this.updateColors(opts);
    }
}
