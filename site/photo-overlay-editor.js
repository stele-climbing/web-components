/*
 * TODO: support undoing something (probably via ctrl-z)
 *
 * TODO: refactor so that states are loadable/unloadable.
 *     : this would encapsulate adding and removing listeners
 *
 * TODO: clicking to remove a point isn't particularly obvious
 *
 * TODO: make line selection more obvious
 *     :  - change color of lines on hover to indicate that they're selectable
 *     :  - use Tab to change selected lines?
 *
 * TODO: make drawing lines more intuitive
 *     :  - extend a (thin?) segment to the cursor in addition to using the crosshairs cursor
 */

const svgCreate = tag =>  document.createElementNS("http://www.w3.org/2000/svg", tag);
const svgAttrs = (el, av) => {
  for (let [a,v] of Object.entries(av))
    if (v === false || v === null || v === undefined) 
      el.removeAttributeNS(null, a);
    else
      el.setAttributeNS(null, a, v);
}

function getSize(url){
  return new Promise(function(resolve, reject){
    const img = document.createElement('img');
    img.onload = (e) => {
      resolve([e.target.naturalWidth, e.target.naturalHeight])
    };
    img.setAttribute('src', url);
  });
}

function svgCoords(clientXY, svg, svgPoint) {
  svgPoint.x = clientXY[0];
  svgPoint.y = clientXY[1];
  const pt = svgPoint.matrixTransform(svg.getScreenCTM().inverse());
  return [pt.x, pt.y];
}

function makeDraggable(el, svg){
  el.addEventListener('mousedown', handleMousedown)
  function handleMousedown(e){
    // e.preventDefault();
    // e.stopPropagation();
    svg.addEventListener('mousemove', handleMove);
    document.addEventListener('mouseup', stopDrag);
  }
  function handleMove(e){
    var event = new Event('drag');
    event.clientX = e.clientX;
    event.clientY = e.clientY;
    return el.dispatchEvent(event);
  }
  function stopDrag(){
    svg.removeEventListener('mousemove', handleMove);
  }
  function teardown(){
    el.removeEventListener('mousedown', handleMousedown)
    svg.removeEventListener('mousemove', handleMove);
    document.removeEventListener('mouseup', teardown);
  }
  el.teardown = teardown;
  return el;
}

export default class PhotoOverlay {
  constructor(v){
    this.climbColor = '#FFF6AA';
    this.areaColor = 'orange';
    this.focusColor = 'yellow';
    this.size = null;
    this.state = {name:'resting'};
    this.climbLayers = {};
    this.areaLayers = {};
    this.setupContainer();
    if (v) this.initPhoto(v).then(() => this.redraw());
  }
  initPhoto(url, cb){
    this.size = null;
    this.imageUrl = null;
    return getSize(url).then(size => {
      this.size     = size;
      this.imageUrl = url;
      this.setup();
    })
  }
  addClimbLayer({id,path,name}){
    this.climbLayers[id] = {path,name};
    this.fireUpdate();
    this.redraw();
  }
  addAreaLayer({id,perimeter,name}){
    this.areaLayers[id] = {perimeter,name};
    this.fireUpdate();
    this.redraw();
  }
  getAreaLayers(){
    let result = [];
    for (let id in this.areaLayers) {
      result.push({
        id: id,
        name: this.areaLayers[id].name,
        perimeter: this.areaLayers[id].perimeter.map(pt => pt.slice()),
      })
    }
    return result;
  }
  getClimbLayers(){
    let result = [];
    for (let id in this.climbLayers) {
      result.push({
        id: id,
        name: this.climbLayers[id].name,
        path: this.climbLayers[id].path.map(pt => pt.slice()),
      })
    }
    return result;
  }
  hasAreaLayer(id){ return Object.hasOwn(this.areaLayers, id); }
  hasClimbLayer(id){ return Object.hasOwn(this.climbLayers, id); }
  setupContainer(){
    //
    // <div class="photo-overlay-editor">
    //   <svg class="photo-overlay-editor__svg">
    //     <image ...>
    //     <g class="photo-overlay-editor__areas"></g>
    //     <g class="photo-overlay-editor__climbs"></g>
    //     <g class="photo-overlay-editor__handles">
    //       <g class="photo-overlay-editor__midpoints"></g>
    //       <g class="photo-overlay-editor__nodes"></g>
    //     </g>
    //     <g class="photo-overlay-editor__tmp"></g>
    //   </svg>
    //   <div class="photo-overlay-editor__message"></div>
    // </div>
    //
    this.container = document.createElement('div');
    this.container.classList.add('photo-overlay-editor');
    this.svg = svgCreate('svg');
    this.svg.classList.add('photo-overlay-editor__svg');
    this.point = this.svg.createSVGPoint();
    this.image = svgCreate('image');
    this.climbsG = svgCreate('g');
    this.climbsG.classList.add('photo-overlay-editor__climbs');
    this.areasG = svgCreate('g');
    this.areasG.classList.add('photo-overlay-editor__areas');
    this.handlesG = svgCreate('g');
    this.handlesG.classList.add('photo-overlay-editor__handles');
    this.midpointsG = svgCreate('g');
    this.midpointsG.classList.add('photo-overlay-editor__midpoints');
    this.nodesG = svgCreate('g');
    this.nodesG.classList.add('photo-overlay-editor__nodes');
    this.tmpG = svgCreate('g');
    this.tmpG.classList.add('photo-overlay-editor__tmp');
    this.statusBar = document.createElement('div');
    this.statusBar.classList.add('photo-overlay-editor__status-bar');
    this.message = document.createElement('div');
    this.message.classList.add('photo-overlay-editor__status-bar-message');
    this.actions = document.createElement('div');
    this.actions.classList.add('photo-overlay-editor__status-bar-actions');
    this.statusBar.appendChild(this.message);
    this.statusBar.appendChild(this.actions);
    this.container.appendChild(this.svg);
    this.container.appendChild(this.statusBar);
    this.svg.appendChild(this.image);
    this.svg.appendChild(this.areasG);
    this.svg.appendChild(this.climbsG);
    this.svg.appendChild(this.handlesG);
    this.svg.appendChild(this.tmpG);
    this.handlesG.appendChild(this.midpointsG);
    this.handlesG.appendChild(this.nodesG);
  }
  isImageReady(){ return !!this.size; }
  setup(){
    let [w, h] = this.size;
    svgAttrs(this.svg, {
      width: w,
      height: h,
      viewBox: `0 0 ${w} ${h}`,
    });
    svgAttrs(this.image, {
      x: 0,
      y: 0,
      width: w,
      height: h,
      href: this.imageUrl,
    });
  }
  clientCoordinates(xy){
    function roundToCover(pct, pixels){ // this should probably be done only once.
      let factor = 0;
      while (pixels > pct * (10 ** factor)) factor++;
      return Math.round(pct * (10 ** factor)) / 10 ** factor;
    }
    const [w,h] = this.size;
    const [x,y] = svgCoords(xy,this.svg,this.point)
    return [
      roundToCover(x/w*100,     w),
      roundToCover((h-y)/h*100, h)
    ];
  }
  redrawAreaLines(){
    let [w, h] = this.size;
    const d = Object.entries(this.areaLayers);
    const e = this.areasG.children;
    const l = Math.max(Object.keys(d).length, e.length);
    for (let i=0; i<l; i++) {
      let el = e[i];
      if (!d[i]) { el.remove(); continue; }
      if (!e[i]) {
        el = svgCreate('path');
        this.areasG.appendChild(el);
      }
      svgAttrs(el, {
        d: 'M ' + d[i][1].perimeter.map(([x,y]) => `${(x/100)*w} ${h-(y/100)*h}`).join(' L '),
        "stroke-width": 5,
        stroke: this.isEditingArea(d[i][0]) ? this.focusColor : this.areaColor,
        fill: 'none',
      });
      el.style.cursor = 'pointer',
      el.onclick = () => { this.startEditingArea(d[i][0]); };
      if (this.isEditingArea(d[i][0])) {
        this.redrawNodesForArea(d[i][0]);
        this.redrawMidpointsForArea(d[i][0]);
      }
    }
  }
  redrawNodesForArea(id){
    const [w,h] = this.size;
    this.handlesG.hidden = false;
    this.nodesG.innerHTML = '';
    let pts = this.areaLayers[id].perimeter; // end is identical to start
    for (let i=1; i<pts.length-1; i++) { 
      let c = svgCreate('circle');
      let [x,y] = pts[i];
      svgAttrs(c, {
        cx: x/100 * w,
        cy: h - (y/100 * h),
        r: 8,
        'stroke-width': 3,
        stroke: 'black',
        fill: this.focusColor,
      });
      c.style.cursor = 'grab';
      this.nodesG.appendChild(c);
      c.addEventListener('click', e => {
        e.preventDefault();
        e.stopPropagation();
        if (pts.length < 5) return;
        this.areaLayers[id].perimeter = pts.slice(0,i).concat(pts.slice(i+1));
        this.fireUpdate();
        this.redraw();
      });
      makeDraggable(c, this.svg);
      c.addEventListener('drag', e => {
        const pt = this.clientCoordinates([e.clientX,e.clientY]);
        this.areaLayers[id].perimeter = pts.slice(0,i).concat([pt]).concat(pts.slice(i+1));
        this.fireUpdate();
        this.redraw();
      });
    }
    let c = svgCreate('circle');
    let [x,y] = pts[0];
    svgAttrs(c, {
      cx: x/100 * w,
      cy: h - (y/100 * h),
      r: 8,
      'stroke-width': 3,
      stroke: "black",
      fill: this.focusColor,
    });
    c.style.cursor = 'grab';
    this.nodesG.appendChild(c);
    c.addEventListener('click', e => {
      e.preventDefault();
      e.stopPropagation();
      if (pts.length < 5) return;
      this.areaLayers[id].perimeter = pts.slice(1,-1).concat([pts[1]]);
      this.fireUpdate();
      this.redraw();
    });
    makeDraggable(c, this.svg);
    c.addEventListener('drag', e => {
      const pt = this.clientCoordinates([e.clientX,e.clientY]);
      this.areaLayers[id].perimeter = [pt].concat(pts.slice(1,-1)).concat([pt]);
      this.fireUpdate();
      this.redraw();
    });
  }
  redrawMidpointsForArea(id){
    const [w,h] = this.size;
    svgAttrs(this.handlesG, {display: false});
    this.midpointsG.innerHTML = '';
    let pts = this.areaLayers[id].perimeter;
    for (let i=1; i<pts.length; i++) { 
      let c = svgCreate('circle');
      let [x1,y1] = pts[i-1];
      let [x2,y2] = pts[i];
      let x = (x1 + x2) / 2;
      let y = (y1 + y2) / 2;
      svgAttrs(c, {
        cx: x/100 * w,
        cy: h - (y/100 * h),
        r: 4,
        'stroke-width': 2,
        stroke: "gray",
        fill: "yellow",
      })
      c.style.cursor = 'grab';
      this.midpointsG.appendChild(c);
      makeDraggable(c, this.svg);
      c.addEventListener('drag', e => {
        const pt = this.clientCoordinates([e.clientX,e.clientY]);
        this.areaLayers[id].perimeter = pts.slice(0,i).concat([pt]).concat(pts.slice(i));
        this.fireUpdate();
        this.redraw();
      });
    }
  }
  redrawClimbLines(){
    let [w, h] = this.size;
    const d = Object.entries(this.climbLayers);
    const e = this.climbsG.children;
    const l = Math.max(Object.keys(d).length, e.length);
    for (let i=0; i<l; i++) {
      let el = e[i];
      if (!d[i]) { el.remove(); continue; }
      if (!e[i]) {
        el = svgCreate('path');
        this.climbsG.appendChild(el);
      }
      svgAttrs(el, {
        d: 'M ' + d[i][1].path.map(([x,y]) => `${(x/100)*w} ${h-(y/100)*h}`).join(' L '),
        "stroke-width": 5,
        stroke: this.isEditingClimb(d[i][0]) ? this.focusColor : this.climbColor,
        fill: 'none',
      });
      el.style.cursor = 'pointer',
      el.onclick = () => { this.startEditingClimb(d[i][0]); };
      if (this.isEditingClimb(d[i][0])) {
        this.redrawNodesForClimb(d[i][0]);
        this.redrawMidpointsForClimb(d[i][0]);
      }
    }
  }
  redrawMidpointsForClimb(id){
    const [w,h] = this.size;
    svgAttrs(this.handlesG, {display: false});
    this.midpointsG.innerHTML = '';
    let pts = this.climbLayers[id].path;
    for (let i=1; i<pts.length; i++) { 
      let c = svgCreate('circle');
      let [x1,y1] = pts[i-1];
      let [x2,y2] = pts[i];
      let x = (x1 + x2) / 2;
      let y = (y1 + y2) / 2;
      svgAttrs(c, {
        cx: x/100 * w,
        cy: h - (y/100 * h),
        r: 4,
        'stroke-width': 2,
        stroke: "gray",
        fill: "yellow",
      })
      c.style.cursor = 'grab';
      this.midpointsG.appendChild(c);
      makeDraggable(c, this.svg);
      c.addEventListener('drag', e => {
        const pt = this.clientCoordinates([e.clientX,e.clientY]);
        this.climbLayers[id].path = pts.slice(0,i).concat([pt]).concat(pts.slice(i));
        this.fireUpdate();
        this.redraw();
      });
    }
  }
  redrawNodesForClimb(id){
    const [w,h] = this.size;
    this.handlesG.hidden = false;
    this.nodesG.innerHTML = '';
    let pts = this.climbLayers[id].path;
    for (let i=0; i<pts.length; i++) { 
      let c = svgCreate('circle');
      let [x,y] = pts[i];
      svgAttrs(c, {
        cx: x/100 * w,
        cy: h - (y/100 * h),
        r: 8,
        'stroke-width': 3,
        stroke: 'black',
        fill: this.focusColor,
      })
      c.style.cursor = 'grab';
      this.nodesG.appendChild(c);
      c.addEventListener('click', e => {
        if (pts.length < 3) return;
        this.climbLayers[id].path = pts.slice(0,i).concat(pts.slice(i+1));
        this.fireUpdate();
        this.redraw();
      });
      makeDraggable(c, this.svg);
      c.addEventListener('drag', e => {
        const pt = this.clientCoordinates([e.clientX,e.clientY]);
        this.climbLayers[id].path = pts.slice(0,i).concat([pt]).concat(pts.slice(i+1));
        this.fireUpdate();
        this.redraw();
      });
    }
  }
  isEditingArea(id){
    return this.state.name === 'editing-area' && this.state.areaId === id;
  }
  startEditingArea(id){
    this.state = {name:'editing-area',areaId:id};
    this.redraw();
  }
  isEditingClimb(id){
    return this.state.name === 'editing-climb' && 
           this.state.climbId === id;
  }
  startDrawingClimb(id,name){
    this.state = {name:'adding-climb',climbId:id,climbName:name,path:[]};
    this.redraw();
  }
  startDrawingArea(id,name){
    this.state = {name:'adding-area',areaId:id,areaName:name,perimeter:[]};
    this.redraw();
  }
  startEditingClimb(id){
    this.state = {name:'editing-climb',climbId:id};
    this.redraw();
  }
  restingState(){
    this.state = {name:'resting'};
    this.redraw();
  }
  setStatusBarMessage(content){
    this.message.innerHTML = '';
    this.message.appendChild(content);
  }
  setStatusBarButtons(buttons){
    this.actions.innerHTML = '';
    for (let btn of buttons) {
      const el = document.createElement('button');
      el.appendChild(document.createTextNode(btn.text))
      el.onclick = (e) => {
        e.preventDefault();
        e.stopPropagation();
        btn.onclick();
      };
      this.actions.appendChild(el);
    }
  }
  setStatusBar(msg, buttons){
    this.setStatusBarMessage(msg);
    this.setStatusBarButtons(buttons);
  }
  redrawStatusBar(){
    let s = this.state;
    let msg = this.statusBar;
    let i = n => {
      let a = document.createElement('i');
      a.appendChild(document.createTextNode(n));
      return a;
    }
    let label = (...segs) => {
      let a = document.createElement('span');
      for (let seg of segs) {
        if (seg instanceof HTMLElement)
          a.appendChild(seg)
        else
          a.appendChild(document.createTextNode(seg));
      }
      return a;
    }
    if (s.name === 'editing-climb') {
      let l = label('editing climb ', i(this.climbLayers[s.climbId].name));
      this.setStatusBar(l, [{
        text: 'remove',
        onclick: () => this.removeClimb(s.climbId),
      }, {
        text: 'redraw',
        onclick: () => {
          let id = s.climbId;
          let name = this.climbLayers[id].name;
          this.removeClimb(id);
          this.startDrawingClimb(id, name);
        },
      }]);
    } else if (s.name === 'editing-area') {
      let l = label('editing area ', i(this.areaLayers[s.areaId].name));
      this.setStatusBar(l, [{
        text: 'remove',
        onclick: () => this.removeArea(s.areaId),
      }, {
        text: 'redraw',
        onclick: () => {
          let id = s.areaId;
          let name = this.areaLayers[id].name;
          this.removeArea(id);
          this.startDrawingArea(id,name);
        }
      }]);
    } else if (s.name === 'adding-area') {
      let l = label('adding area ', i(s.areaName), '. click to start drawing the perimeter');
      this.setStatusBar(l, [{
        text: 'cancel',
        onclick: () => this.removeArea(s.areaId),
      }]);
    } else {
      let msg = document.createTextNode('add an item, or select something to edit');
      this.setStatusBar(msg, []);
    }
  }
  removeArea(id){
    delete this.areaLayers[this.state.areaId];
    this.state = {name:'resting'};
    this.fireUpdate();
    this.redraw();
  }
  removeClimb(id){
    delete this.climbLayers[this.state.climbId];
    this.state = {name:'resting'};
    this.fireUpdate();
    this.redraw();
  }
  getValue(){
    return {
      areas: this.getAreaLayers(),
      climbs: this.getClimbLayers(),
    }
  }
  fireUpdate(){ this.onupdate && this.onupdate(this.getValue()); }
  asSvgCoords(points){
    let [w,h] = this.size;
    return points.map(([x,y]) => [w/100*x,h-(y/100*h)]);
  }
  redrawAddingClimb(){
    this.tmpG.innerHTML = '';
    this.image.onclick = (e) => {
      const pt = this.clientCoordinates([e.clientX,e.clientY]);
      this.state.perimeter.push(pt);
      this.redraw();
    }
    let p = svgCreate('path');
    svgAttrs(p, {
      d: 'M' + this.asSvgCoords(this.state.perimeter).map(pt => pt.join(' ')).join(' L '),
      'stroke-width': 5,
      stroke: this.climbColor,
      fill: 'none',
    });
    this.tmpG.appendChild(p);
    this.svg.style.cursor = 'crosshair';
  }
  redrawAddingClimb(){
    this.tmpG.innerHTML = '';
    let pat = this.state.path;
    this.image.onclick = (e) => {
      const pt = this.clientCoordinates([e.clientX,e.clientY]);
      if (pat.length > 1) {
        this.addClimbLayer({
          id: this.state.climbId,
          name: this.state.climbName,
          path: pat.concat([pt]),
        });
        this.state = {name:'editing-climb',climbId:this.state.climbId};
        this.redraw();
      } else {
        this.state.path.push(pt);
      }
      this.redraw();
    }
    let p = svgCreate('path');
    svgAttrs(p, {
      d: 'M' + this.asSvgCoords(pat).map(pt => pt.join(' ')).join(' L '),
      'stroke-width': 5,
      stroke: this.climbColor,
      fill: 'none',
    });
    this.tmpG.appendChild(p);
    if (pat.length) {
      let c = svgCreate('circle');
      svgAttrs(c, {
        cx: this.asSvgCoords(pat)[0][0],
        cy: this.asSvgCoords(pat)[0][1],
        r: 8,
        'stroke-width': 5,
        'stroke': 'black',
        'fill': 'black',
      });
      c.style.cursor = 'pointer';
      this.tmpG.appendChild(c);
    }
    this.svg.style.cursor = 'crosshair';
  }
  redrawAddingArea(){
    this.tmpG.innerHTML = '';
    let perim = this.state.perimeter;
    this.image.onclick = (e) => {
      const pt = this.clientCoordinates([e.clientX,e.clientY]);
      this.state.perimeter.push(pt);
      this.redraw();
    }
    let p = svgCreate('path');
    svgAttrs(p, {
      d: 'M' + this.asSvgCoords(perim).map(pt => pt.join(' ')).join(' L '),
      'stroke-width': 5,
      stroke: this.focusColor,
      fill: 'none',
    });
    this.tmpG.appendChild(p);
    let c = svgCreate('circle');
    svgAttrs(c, {
      cx: this.asSvgCoords(perim)[0][0],
      cy: this.asSvgCoords(perim)[0][1],
      r: 8,
      'stroke-width': 5,
      'stroke': 'black',
      'fill': (perim.length > 2) ? this.focusColor : this.areaColor,
    });
    c.style.cursor = 'pointer';
    if (perim.length > 2) {
      c.onclick = () => {
        this.addAreaLayer({
          id: this.state.areaId,
          name: this.state.areaName,
          perimeter: perim.concat([perim[0].slice()]),
        });
        this.state = {name:'editing-area',areaId:this.state.areaId};
        this.redraw();
      }
    }
    this.tmpG.appendChild(c);
    this.svg.style.cursor = 'crosshair';
  }
  redraw(){
    if (!this.isImageReady()) return;
    this.redrawClimbLines();
    this.redrawAreaLines();
    this.redrawStatusBar();
    if (this.state.name === 'editing-climb') {
      this.tmpG.innerHTML = '';
      this.image.onclick = (e) => {
        const pt = this.clientCoordinates([e.clientX,e.clientY]);
        this.climbLayers[this.state.climbId].path.push(pt);
        this.fireUpdate();
        this.redraw();
      }
      this.svg.style.cursor = 'crosshair';
    } else if (this.state.name === 'editing-area') {
      this.tmpG.innerHTML = '';
      this.svg.style.cursor = 'default';
      delete this.image.onclick;
    } else if (this.state.name === 'adding-climb') {
      delete this.image.onclick;
      this.redrawAddingClimb();
    } else if (this.state.name === 'adding-area') {
      delete this.image.onclick;
      this.redrawAddingArea();
    } else {
      this.tmpG.innerHTML = '';
      this.svg.style.cursor = 'default';
      svgAttrs(this.handlesG, {display: 'none'});
      delete this.image.onclick;
    }
  }
}

