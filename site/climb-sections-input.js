/*
 * Here's the layout:
 *
 * <div class="climb-sections-input">
 *     <div class="climb-sections-input--segments">
 *         <div class="climb-sections-input--segment">
 *             <input  class="climb-sections-input--starting-at" ...>
 *             <input  class="climb-sections-input--description" ...>
 *             <button class="climb-sections-input--remove">
 *             <button class="climb-sections-input--divide">
 *         </div>
 *     </div>
 *     <div class="climb-sections-input--general">
 *         <button class="climb-sections-input--clear">
 *         <button class="climb-sections-input--start">
 *     </div>
 * </div>
 *
 */
export default class ClimbTerrainInput {
    overhangMax = +90
    overhangMin = -90
    constructor(){
        this.container = document.createElement('div');
        this.segments  = document.createElement('div');
        this.general   = document.createElement('div');
        this.clear     = document.createElement('button');
        this.start     = document.createElement('button');
        this.container.classList.add('climb-sections-input');
        this.segments .classList.add('climb-sections-input--segments');
        this.general  .classList.add('climb-sections-input--general');
        this.clear    .classList.add('climb-sections-input--clear');
        this.start    .classList.add('climb-sections-input--start');
        this.clear.textContent = 'clear';
        this.start.textContent = 'add segment';
        this.container.appendChild(this.segments);
        this.container.appendChild(this.general);
        this.general.appendChild(this.clear);
        this.general.appendChild(this.start);
        this.segments.appendChild(this.createSegmentElement())
    }
    static splitSegment(segments, startingAt){
        //NOTE: perhaps this should only split if there is enough space height=2*minstep
        let result = [];
        for (let i = 0; i<segments.length; i++) {
            if (segments[i].startingAt === startingAt) {
                const nextStart = (i+1 < segments.length) ? segments[i+1].startingAt : 1;
                const startingAt = segments[i].startingAt;
                const description = segments[i].description;
                result.push({startingAt,                               description});
                result.push({startingAt: (startingAt + nextStart) / 2, description});
            } else {
                result.push({...segments[i]});
            }
        }
        return result;
    }
    static dropSegment(segments, startingAt){
        const s2 = segments.filter(seg => seg.startingAt !== startingAt).map(seg => ({...seg}));
        if (!s2.length) {
            return null;
        } else {
            s2[0].startingAt = 0; // in case we removed the first segment
            return s2;
        }
    }
    createSegmentElement(){
        const segment = document.createElement('div');
        const startingAt = document.createElement('input');
        const description = document.createElement('input');
        const divide  = document.createElement('button');
        const remove  = document.createElement('button');
        segment.classList.add('climb-sections-input--segment');
        startingAt.classList.add('climb-sections-input--starting-at');
        description.classList.add('climb-sections-input--description');
        divide .classList.add('climb-sections-input--divide');
        remove .classList.add('climb-sections-input--remove');
        startingAt.setAttribute('type', 'number');
        startingAt.setAttribute('step', '0.05');
        startingAt.setAttribute('min',  '0.00');
        startingAt.setAttribute('max',  '0.95');
        description.setAttribute('type',        'text');
        description.setAttribute('placeholder', '???????????');
        divide.textContent  = "split"; // "&#9986;" // (scissors) // &plus;
        remove.textContent = "remove"; // "&times;"
        divide.onclick = (e) => {
            let a = this.getValue()
            let b = ClimbTerrainInput.splitSegment(a, Number(startingAt.value));
            this.redraw(b);
            this.fireInput();
        }
        remove.onclick = (e) => {
            let a = this.getValue()
            let b = ClimbTerrainInput.dropSegment(a, Number(startingAt.value));
            this.redraw(b);
            this.fireInput();
        }
        startingAt.oninput = (e) => {
            this.redraw(this.getValue());
            this.fireInput();
        }
        description.oninput = (e) => {
            this.redraw(this.getValue());
            this.fireInput();
        }
        segment.appendChild(startingAt);
        segment.appendChild(description);
        segment.appendChild(divide);
        segment.appendChild(remove);
        return segment;
    }
    fireInput(){
        if (this.oninput) this.oninput(this.getValue());
    }
    static updateSegmentElement(el, segmentData, context) {
        const segment = el; 
        const startingAt  = el.querySelector('.climb-sections-input--starting-at');
        const description = el.querySelector('.climb-sections-input--description');
        // const divide  = el.querySelector('.climb-sections-input--divide');
        // const remove  = el.querySelector('.climb-sections-input--remove');
        startingAt.value = segmentData.startingAt;
        if (context.prevStart === null) {
          startingAt.value = 0;
          startingAt.setAttribute('disabled', 'disabled');
        } else {
          startingAt.removeAttribute('disabled');
        }
        const min = (context.prevStart !== null) ? (context.prevStart+0.05) : 0;
        const max = (context.nextStart !== null) ? (context.nextStart-0.05) : 0.95;
        startingAt.setAttribute('min', min);
        startingAt.setAttribute('max', max);
        description.value = segmentData.description;
    }
    redrawInputs(data){
        const elements = [].slice.call(this.segments.querySelectorAll('.climb-sections-input--segment')).reverse();
        const end = Math.max(data.length, elements.length);
        for (let i=0; i<end; i++) {
            let e = elements[i];
            let d = data[i];
            if (!d) {
                e.remove();
            } else {
                if (!e) {
                    e = this.createSegmentElement();
                    if (this.segments.children.length) 
                        this.segments.insertBefore(e, this.segments.children[0]);
                    else
                        this.segments.appendChild(e);
                }
                //TODO: update
                ClimbTerrainInput.updateSegmentElement(e, data[i], {
                    prevStart: (i === 0)           ? null : data[i-1].startingAt,
                    nextStart: (i+1===data.length) ? null : data[i+1].startingAt,
                })
            }
        }
        return null;
    }
    redraw(v){
        if (v && v.length) {
            this.general.setAttribute('hidden', 'hidden');
            this.segments.removeAttribute('hidden');
            this.redrawInputs(v);
        } else {
            this.segments.setAttribute('hidden', 'hidden');
            this.general.removeAttribute('hidden');
        }
    }
    setValue(v){
        this.redraw(v);
    }
    getValue(){
        const q = this.segments.querySelectorAll('.climb-sections-input--segment');
        const elements = [].slice.call(q).reverse();
        return elements.map(e => {
            const startEl = e.querySelector('.climb-sections-input--starting-at');
            const descEl  = e.querySelector('.climb-sections-input--description');
            return {
                startingAt:  Number(startEl.value),
                description: descEl.value,
            }
        })
    }
    update(){
        let valid = true;
        if (valid && this.onchange)
            this.onchange(this.getValue());
        this.redraw();
        return null;
    }
}

