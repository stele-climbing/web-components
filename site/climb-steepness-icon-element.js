class ClimbSteepnessIconElement extends HTMLElement {

    degreesOverhung = 0
    width           = 50
    height          = 50
    borderWidth     = 3
    borderRadius    = 5
    borderColor     = '#222222' 
    rockColor       = '#A0522D' // sienna
    skyColor        = '#B0E0E6' // powder blue

    static observedAttributes = [
        'degrees-overhung',
        'height', 'width',
        'border-width', 'border-radius', 'border-color',
        'sky-color', 'rock-color',
    ];

    constructor() {
        super();
        const shadow = this.attachShadow({mode:'closed'});

        /*
         * <svg class="climb-steepness-icon">
         *   <defs>
         *     <clipPath id="scene-clip"><rect ...></rect></clipPath>
         *   </defs>
         *   <g clip-path="url(#climb-steepness-icon__scene-clip-12345)">
         *     <rect ...></rect>  <!-- sky, cover the whole background -->
         *     <rect ...></rect>  <!-- rock, rect (rotated to cover the wall) -->
         *   </g>
         *   <rect ...></rect>    <!-- decorating/outlining -->
         * </svg>
         */
        this.svg    = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        this.border = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        this.sky    = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        this.rock   = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        this.defs   = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
        this.scene  = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        this.clipp  = document.createElementNS('http://www.w3.org/2000/svg', 'clipPath');
        this.clipr  = document.createElementNS('http://www.w3.org/2000/svg', 'rect');

        // default fill is black
        this.border.style.fill = "none";

        // set up the clipping
        this.clipp.setAttributeNS(null, 'id', `scene-clip`);
        this.scene.setAttributeNS(null, 'clip-path', `url(#scene-clip)`);

        this.svg  .appendChild(this.defs);
        this.defs .appendChild(this.clipp);
        this.clipp.appendChild(this.clipr);
        this.scene.appendChild(this.sky)
        this.scene.appendChild(this.rock);
        this.svg  .appendChild(this.scene);
        this.svg  .appendChild(this.border);
        shadow.appendChild(this.svg);
/*
        this.width = 50;
        this.height = 50;
        this.borderWidth = 4;
        this.borderRadius = 8;
        this.degreesOverhung = 0;
        this.skyColor = 'blue';
        this.rockColor = 'brown';
        this.borderColor = 'black';
*/
        this.updateContainerSize();
        this.updateInternalLayout();
        this.updateColors();
    }
    updateContainerSize(){
        this.svg.setAttributeNS(null, 'width',   this.width);
        this.svg.setAttributeNS(null, 'height',  this.height);
        this.svg.setAttributeNS(null, 'viewBox', `0 0 ${this.width} ${this.height}`);
    }
    updateInternalLayout(){
        // these should affect nothing outside the element
        const d           = this.degreesOverhung;
        const borderWidth = this.borderWidth;
        const width       = this.width;
        const height      = this.height;
        const diagonal    = Math.sqrt((width+borderWidth)**2+(height+borderWidth)**2);
        this.sky.setAttributeNS(null, 'width',  width);
        this.sky.setAttributeNS(null, 'height', height);
        this.sky.setAttributeNS(null, 'x', 0);
        this.sky.setAttributeNS(null, 'y', 0);
        this.rock.setAttributeNS(null, 'width',  diagonal);
        this.rock.setAttributeNS(null, 'height', diagonal);
        this.rock.setAttributeNS(null, 'x', width / 2);
        this.rock.setAttributeNS(null, 'y', (height-diagonal)/2);
        this.rock.setAttributeNS(null, 'transform', `rotate(${d*-1} ${width/2} ${height/2})`);
        this.clipr.setAttributeNS(null, 'width',  width - borderWidth);
        this.clipr.setAttributeNS(null, 'height', height - borderWidth);
        this.clipr.setAttributeNS(null, 'x', borderWidth / 2);
        this.clipr.setAttributeNS(null, 'y', borderWidth / 2);

        this.border.setAttributeNS(null, 'width',  width - borderWidth);
        this.border.setAttributeNS(null, 'height', height - borderWidth);
        this.border.setAttributeNS(null, 'x', borderWidth / 2);
        this.border.setAttributeNS(null, 'y', borderWidth / 2);

        this.border.style.strokeWidth = this.borderWidth;

        const r = this.borderRadius;
        this.clipr.setAttributeNS(null, 'rx', r);
        this.clipr.setAttributeNS(null, 'ry', r);
        this.border.setAttributeNS(null, 'rx', r);
        this.border.setAttributeNS(null, 'ry', r);
    }
    updateColors(){
        // these are entirely cosmetic and can be updated independently
        this.border.style.stroke = this.borderColor;
        this.sky.style.fill      = this.skyColor;
        this.rock.style.fill     = this.rockColor;
    }
    attributeChangedCallback(name, oldValue, newValue) {
        switch (name) {
            case 'degrees-overhung':
                this.degreesOverhung = Number(newValue); 
                this.updateInternalLayout();
                break;
            case 'height':
                this.height = Number(newValue); 
                this.updateContainerSize();
                this.updateInternalLayout();
                break;
            case 'width':
                this.width = Number(newValue); 
                this.updateContainerSize();
                this.updateInternalLayout();
                break;
            case 'border-radius':
                this.borderRadius = Number(newValue); 
                this.updateInternalLayout();
                break;
            case 'border-width':
                this.borderWidth = newValue; 
                this.updateInternalLayout();
                break;
            case 'border-radius':
                this.borderRadius = newValue; 
                this.updateInternalLayout();
                break;
            case 'sky-color':
                this.skyColor = newValue; 
                this.updateColors();
                break;
            case 'rock-color':
                this.rockColor = newValue; 
                this.updateColors();
                break;
            case 'border-color':
                this.borderColor = newValue; 
                this.updateColors();
                break;
        }
    }
}

customElements.define("climb-steepness-icon", ClimbSteepnessIconElement);
