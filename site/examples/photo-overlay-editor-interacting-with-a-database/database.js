export default class Database {
    constructor(){
        this.photos = {
           "1011": {
               url:    './the-pearl-in-red-rocks.jpg',
               climbs: [{id: '1001', points: [[32,37],[30,60],[21.9,83.3],[23,87],[26.8,90.4]]},
                        {id: '1002', points: [[45,50],[45,80]]},
                        {id: '1003', points: [[50,40],[70,70]]}],
               areas:  [{id: '1004', points: [[6.484,20.57],[39.47,16.29],[48.3,10.14],
                                              [69.35,10.28],[77.57,23.91],[81.68,72.84],
                                              [76.07,83.1],[57.12,92.8],[43.68,97.7],
                                              [35.06,97.6],[19.62,88.5],[20.12,74.18],
                                              [15.81,65.36],[9.191,48.24],[6.484,20.57]]}]
           }
        };
        this.climbs = {
            '1001': {name: 'The Pearl',         grade: 'V3'},
            '1002': {name: 'The Clam Bumper',   grade: '??'},
            '1003': {name: 'Clam Bumper Right', grade: '??'}
        };
        this.areas  = {
            '1004': {name: 'The Pearl Boulder'},
        };
    }
    async connect(){
        console.info('Database#connect doesn\'t actually do anything');
        return
    }
    setPhotoClimbLayers(id, climbs){
        this.photos[id].climbs = climbs.map(v => ({points:v.path,id:v.id}));
    }
    setPhotoAreaLayers(id, areas){
        this.photos[id].areas = areas.map(v => ({points:v.perimeter,id:v.id}));
    }
    getArea(id){  return {id, ...this.areas[id]};  }
    getClimb(id){ return {id, ...this.climbs[id]}; }
    getPhoto(id){ return {id, ...this.photos[id]}; }
    search(term){
        const termLower = term.toLowerCase();
        const matches = [];
        for (let [id, {name, grade}] of Object.entries(this.climbs))
            if (0 <= name.toLowerCase().indexOf(termLower)) 
                matches.push({kind:'climb',name,grade,id});
        for (let [id, {name}] of Object.entries(this.areas))
            if (0 <= name.toLowerCase().indexOf(termLower)) 
                matches.push({kind:'area',name,id});
        return matches;
    }
    getAsTables(){
        const climbs = []
        const areas = []
        const photos = []
        const climbLayers = []
        const areaLayers = []
        for (let [id, {name, grade}] of Object.entries(this.climbs))
            climbs.push([id,name,grade])
        for (let [id, {name}] of Object.entries(this.areas))
            areas.push([id,name])
        for (let [id, photo] of Object.entries(this.photos)) {
            photos.push([id, photo.url])
            for (let layer of photo.climbs) 
                climbLayers.push([id, layer.points, layer.id])
            for (let layer of photo.areas) 
                areaLayers.push([id, layer.points, layer.id])
        }
        return {climbs,areas,photos,climbLayers,areaLayers};
    }
}
