export default class ClimbSteepnessEditor {
  static _counter = 100;
  static nextId(){
    let i = ClimbSteepnessEditor._counter;
    ClimbSteepnessEditor._counter += 1;
    return i;
  }
  constructor(){
    this._id = ClimbSteepnessEditor.nextId();
    this.r = 100;
    this.overhangMax = +90;
    this.overhangMin = -90;
    this.borderColor = '#CCC';
    this.backgroundColor = '#FFF';
    this.rayColor = '#000';
    this.rayWidth = 6;
    this.d = []
    const cx = this.r;
    const cy = this.r;
    const ohMinRun = Math.sin(this.overhangMin * (Math.PI/180));
    const ohMinRise = Math.cos(this.overhangMin * (Math.PI/180));
    const ohMaxRun = Math.sin(this.overhangMax * (Math.PI/180));
    const ohMaxRise = Math.cos(this.overhangMax * (Math.PI/180));
    this.figure = document.createElement('figure');
    this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    this.svg.style.overflow = 'visible';
    this.defs = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
    this.ringsClip = document.createElementNS('http://www.w3.org/2000/svg', 'clipPath');
    this.ringsClipPolyline = document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
    this.raysg = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.raysg.classList.add('climb-steepness-rays');
    this.ringsg = document.createElementNS('http://www.w3.org/2000/svg', 'g');
    this.ringsg.classList.add('climb-steepness-rings');
    this.ringsBg = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    this.ringsg.appendChild(this.ringsBg);
    this.ringsBg.setAttribute('cx', cx);
    this.ringsBg.setAttribute('cy', cy);
    this.ringsBg.setAttribute('r', this.r);
    this.ringsBg.setAttribute('fill', this.backgroundColor);
    this.ringsBg.setAttribute('stroke', this.borderColor);
    this.ringsBg.setAttribute('stroke-width', '3');
    this.figcaption = document.createElement('figcaption');
    this.figure.appendChild(this.svg);
    this.figure.appendChild(this.figcaption);
    this.svg.appendChild(this.defs);
    this.defs.appendChild(this.ringsClip);
    this.ringsClip.setAttribute('id', `rings-clip_${this._id}`);
    this.ringsg.setAttribute('clip-path', `url(#rings-clip_${this._id})`);
    this.ringsClip.appendChild(this.ringsClipPolyline);
    this.ringsClipPolyline.setAttribute('points', [
      [-5,                     -5],
      [cx - (ohMaxRun*(this.r*1.1)), cy - (ohMaxRise*(this.r*1.1))],
      [cx,                     cy],
      [cx - (ohMinRun*(this.r*1.1)), cy - (ohMinRise*(this.r*1.1))],
      [this.r*2,               -5],
    ].map(p => p.join(',')).join(' '))

    const rayOhMin = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    rayOhMin.setAttribute('x1', cx);
    rayOhMin.setAttribute('y1', cy);
    rayOhMin.setAttribute('x2', cx - (ohMinRun * this.r));
    rayOhMin.setAttribute('y2', cy - (ohMinRise * this.r));
    rayOhMin.setAttribute('stroke', this.borderColor);
    rayOhMin.setAttribute('stroke-width', '3');
    rayOhMin.setAttribute('stroke-linecap', 'round');

    const rayOhMax = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    rayOhMax.setAttribute('x1', cx);
    rayOhMax.setAttribute('y1', cy);
    rayOhMax.setAttribute('x2', cx - (ohMaxRun * this.r));
    rayOhMax.setAttribute('y2', cy - (ohMaxRise * this.r));
    rayOhMax.setAttribute('stroke', this.borderColor);
    rayOhMax.setAttribute('stroke-width', '3');
    rayOhMax.setAttribute('stroke-linecap', 'round');

    this.svg.appendChild(this.ringsg);
    this.svg.appendChild(rayOhMin);
    this.svg.appendChild(rayOhMax);
    this.svg.appendChild(this.raysg);

    this.redraw();
  }
  getRootElement(){
    return this.figure;
  }
  setSteepness(pts){
    this.d = [];
    for (let pt of pts)
      this.d.push([+pt.startingAt, +pt.degreesOverhung])
    this.update();
  }
  getDecorated(){
    let v = [];
    for (let i=0; i<this.d.length; i++) {
      let o = this.d[i][1];
      let a = this.d[i][0];
      let b = this.d[i+1] ? this.d[i+1][0] : 1;
      v.push({
        startingAt: a,
        stoppingAt: b,
        degreesOverhung: o,
        radiansOverhung: o*(Math.PI/180),
      });
    }
    return v;
  }
  redrawSvg(){
    let r = this.r;
    let cx = r;
    let cy = r;
    const segments = this.getDecorated() || [];
    const rays = this.raysg.querySelectorAll('.climb-steepness-ray');
    const rings = this.ringsg.querySelectorAll('.climb-steepness-ring');
    const end = Math.max(segments.length,rays.length);
    for (let i=0; i<end; i++) {
      let seg = segments[i];
      let ray = rays[i];
      let ring = rings[i];
      if (!seg) {
        ray.remove();
        ring.remove();
        continue;
      }
      if (!ray) {
        ray = document.createElementNS('http://www.w3.org/2000/svg', 'line');
        ray.classList.add('climb-steepness-ray');
        ray.setAttribute('fill', 'none');
        ray.setAttribute('stroke', this.rayColor);
        ray.setAttribute('stroke-width', this.rayWidth);
        ray.setAttribute('stroke-linecap', 'round');
        ray.classList.add('climb-steepness-ray');
        this.raysg.appendChild(ray);
        ring = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        ring.setAttribute('fill', 'none');
        ring.setAttribute('stroke', 'rgba(0,0,0,0.5)');
        ring.setAttribute('stroke-width', '1');
        ring.setAttribute('stroke-dasharray', '1 2');
        ring.classList.add('climb-steepness-ring');
        this.ringsg.appendChild(ring);
      }
      ring.setAttribute('cx', cx);
      ring.setAttribute('cy', cy);
      let run = Math.sin(seg.radiansOverhung);
      let rise = Math.cos(seg.radiansOverhung);
      let start = seg.startingAt;
      let stop = seg.stoppingAt;
      ray.setAttribute('x1', cx - (start*run)*r);
      ray.setAttribute('y1', cy - (start*rise)*r);
      ray.setAttribute('x2', cx - (stop*run)*r);
      ray.setAttribute('y2', cy - (stop*rise)*r);
      ring.setAttribute('r', stop * r);
    }
    return null;
  }
  redrawInputs(){
    const segments = this.getDecorated() || [];
    const inputs = [].slice.call(this.figcaption.querySelectorAll('.climb-steepness-segment-inputs')).reverse();
    const end = Math.max(segments.length,inputs.length);
    for (let i=0; i<end; i++) {
      let el = inputs[i];
      let seg = segments[i];
      if (!seg) {
        el.remove();
        continue;
      }
      if (!el) {
        el = document.createElement('div');
        el.classList.add('climb-steepness-segment-inputs');
        let start = document.createElement('input');
        start.setAttribute('type', 'number');
        start.setAttribute('size', '5');
        start.setAttribute('step', '0.05');
        start.setAttribute('min', '0.00');
        start.setAttribute('max', '0.95');
        start.classList.add('climb-steepness-segment-start');
        let deg = document.createElement('input');
        deg.setAttribute('type', 'number');  //TODO: switch to range? type=range
        deg.setAttribute('size', '5');
        deg.setAttribute('step', '5');
        deg.setAttribute('min', '-90');
        deg.setAttribute('max', '100');
        deg.classList.add('climb-steepness-segment-degrees');
        let split = document.createElement('button');
        split.classList.add('climb-steepness-segment-split');
        split.innerHTML = 'split'; // '&#9986;'; (scissors) // &plus;
        let rm = document.createElement('button');
        rm.classList.add('climb-steepness-segment-remove');
        rm.innerHTML = '&times;';
        el.appendChild(start);
        el.appendChild(deg);
        el.appendChild(split);
        el.appendChild(rm);
        if (this.figcaption.firstChild)
          this.figcaption.insertBefore(el, this.figcaption.firstChild);
        else
          this.figcaption.appendChild(el);
      }
      let start = el.querySelector('.climb-steepness-segment-start');
      let deg = el.querySelector('.climb-steepness-segment-degrees');
      let split = el.querySelector('.climb-steepness-segment-split');
      let rm = el.querySelector('.climb-steepness-segment-remove');
      el.onmouseenter = () => {
        this.raysg.querySelectorAll('.climb-steepness-ray')[i].setAttribute('stroke', 'yellow');
      };
      el.onmouseleave = () => {
        this.raysg.querySelectorAll('.climb-steepness-ray')[i].setAttribute('stroke', this.rayColor);
      };
      start.oninput = (e) => this.setSegmentStartingAt(i,e.target.value);
      deg.oninput = (e) => this.setSegmentDegreesOverhung(i,e.target.value);
      split.onclick = (e) => this.splitSegment(i);
      rm.onclick = (e) => this.removeSegment(i);
      start.value = seg.startingAt;
      deg.value = seg.degreesOverhung;
    }
    return null;
  }
  setSegmentStartingAt(i, v){
    this.d[i][0] = +v;
    this.update();
  }
  setSegmentDegreesOverhung(i, v){
    this.d[i][1] = +v;
    this.update();
  }
  splitSegment(isplit){
    let e = [];
    for (let i=0;i<this.d.length;i++) {
      e.push([this.d[i][0],this.d[i][1]]);
      if (i === isplit) {
         let nextStart = this.d[i+1] ? this.d[i+1][0] : 1;
         e.push([(this.d[i][0]+nextStart)/2,this.d[i][1]]);
      }
    }
    this.d = e;
    this.update();
  }
  removeSegment(iremove){
    let e = [];
    for (let i=0;i<this.d.length;i++) {
      if (i !== iremove)
        e.push([this.d[i][0],this.d[i][1]]);
    }
    this.d = e;
    this.update();
  }
  redraw(){
    this.redrawSvg();
    this.redrawInputs();
  }
  getValue(){
    return this.d.map(([startingAt, degreesOverhung]) => ({
      startingAt,
      degreesOverhung
    }));
  }
  update(){
    let valid = true;
    if (valid && this.onchange)
      this.onchange(this.getValue());
    this.redraw();
    return null;
  }
}

